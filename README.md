# go语言的eureka-client

### 使用方式
```go
package ctx

import (
	"gitee.com/gaojh/eureka-client/eureka"
	"gitee.com/gaojh/eureka-client/feign"
	"sync"
)

type ctx struct {
	eurekaClient *eureka.Client
}

var me *ctx
var once sync.Once

func Me() *ctx {
	once.Do(func() {
		client := eureka.NewClient(&eureka.Config{
			DefaultZone:           "eureka.url",
			App:                   "eureka.app.name",
			Port:                  8080,
			RenewalIntervalInSecs: 10,
			DurationInSecs:        30,
		})
		me = &ctx{
			eurekaClient: client,
		}
	})

	return me
}

func (c *ctx) Start() {
	c.eurekaClient.Start()
}

func (c *ctx) FeignClient() *feign.Client {
	client := feign.NewClient(me.eurekaClient)
	client.Header("clientType", "1")
	return client
}
```